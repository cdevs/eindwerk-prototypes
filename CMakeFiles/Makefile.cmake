# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

# The generator used is:
SET(CMAKE_DEPENDS_GENERATOR "Unix Makefiles")

# The top level Makefile was generated from the following files:
SET(CMAKE_MAKEFILE_DEPENDS
  "CMakeCache.txt"
  "CMakeFiles/2.8.12.2/CMakeCXXCompiler.cmake"
  "CMakeFiles/2.8.12.2/CMakeSystem.cmake"
  "CMakeLists.txt"
  "src/CMakeLists.txt"
  "src/modelloader/CMakeLists.txt"
  "src/serial/Basic-Serialization/CMakeLists.txt"
  "src/serial/CMakeLists.txt"
  "src/serial/GameOfLifeSerialization/CMakeLists.txt"
  "src/threading/CMakeLists.txt"
  "src/threading/pool_threading/CMakeLists.txt"
  "src/threading/queue_mutex/CMakeLists.txt"
  "src/threading/simple_threading/CMakeLists.txt"
  "/usr/share/cmake-2.8/Modules/CMakeCXXInformation.cmake"
  "/usr/share/cmake-2.8/Modules/CMakeCommonLanguageInclude.cmake"
  "/usr/share/cmake-2.8/Modules/CMakeGenericSystem.cmake"
  "/usr/share/cmake-2.8/Modules/CMakeSystemSpecificInformation.cmake"
  "/usr/share/cmake-2.8/Modules/Compiler/GNU-CXX.cmake"
  "/usr/share/cmake-2.8/Modules/Compiler/GNU.cmake"
  "/usr/share/cmake-2.8/Modules/Platform/Linux-GNU-CXX.cmake"
  "/usr/share/cmake-2.8/Modules/Platform/Linux-GNU.cmake"
  "/usr/share/cmake-2.8/Modules/Platform/Linux.cmake"
  "/usr/share/cmake-2.8/Modules/Platform/UnixPaths.cmake"
  )

# The corresponding makefile is:
SET(CMAKE_MAKEFILE_OUTPUTS
  "Makefile"
  "CMakeFiles/cmake.check_cache"
  )

# Byproducts of CMake generate step:
SET(CMAKE_MAKEFILE_PRODUCTS
  "CMakeFiles/CMakeDirectoryInformation.cmake"
  "src/CMakeFiles/CMakeDirectoryInformation.cmake"
  "src/modelloader/CMakeFiles/CMakeDirectoryInformation.cmake"
  "src/serial/CMakeFiles/CMakeDirectoryInformation.cmake"
  "src/serial/Basic-Serialization/CMakeFiles/CMakeDirectoryInformation.cmake"
  "src/serial/GameOfLifeSerialization/CMakeFiles/CMakeDirectoryInformation.cmake"
  "src/threading/CMakeFiles/CMakeDirectoryInformation.cmake"
  "src/threading/pool_threading/CMakeFiles/CMakeDirectoryInformation.cmake"
  "src/threading/queue_mutex/CMakeFiles/CMakeDirectoryInformation.cmake"
  "src/threading/simple_threading/CMakeFiles/CMakeDirectoryInformation.cmake"
  )

# Dependency information for all targets:
SET(CMAKE_DEPEND_INFO_FILES
  "src/modelloader/CMakeFiles/modelloader_tests.dir/DependInfo.cmake"
  "src/modelloader/CMakeFiles/testmodel1.dir/DependInfo.cmake"
  "src/modelloader/CMakeFiles/testmodel2.dir/DependInfo.cmake"
  "src/serial/Basic-Serialization/CMakeFiles/serial_binary.dir/DependInfo.cmake"
  "src/serial/Basic-Serialization/CMakeFiles/serial_json.dir/DependInfo.cmake"
  "src/serial/Basic-Serialization/CMakeFiles/serial_xml.dir/DependInfo.cmake"
  "src/serial/GameOfLifeSerialization/CMakeFiles/serial_gol.dir/DependInfo.cmake"
  "src/threading/pool_threading/CMakeFiles/threading_pool.dir/DependInfo.cmake"
  "src/threading/queue_mutex/CMakeFiles/threading_queue_mutex.dir/DependInfo.cmake"
  "src/threading/simple_threading/CMakeFiles/threading_simple.dir/DependInfo.cmake"
  )
