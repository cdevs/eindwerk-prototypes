//============================================================================
// Name        : Serializing.cpp
// Author      : 
// Version     :
// Copyright   : 
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <fstream>
#include "cereal/cereal.hpp"
#include <cereal/archives/binary.hpp>
#include "cereal/types/map.hpp"
#include "cereal/types/vector.hpp"
#include "cereal/types/memory.hpp"
#include "cereal/types/string.hpp"
#include "cereal/types/base_class.hpp"
#include <string>
#include <memory>

#include <sstream>
#include "Serializable.h"

#include "gtest/gtest.h"

using namespace std;

TEST(DataSerializingTest, String) {
	stringstream ss;
	std::string s_in, s_out;

	s_in = "Test";
	{
		cereal::BinaryOutputArchive oarchive(ss);
		oarchive(s_in);
	}
	{
		cereal::BinaryInputArchive iarchive(ss); // Create an input archive
		iarchive(s_out); // Read the data from the archive
	}
	EXPECT_EQ(s_in, s_out);

	s_in = "";
	{
		cereal::BinaryOutputArchive oarchive(ss);
		oarchive(s_in);
	}
	{
		cereal::BinaryInputArchive iarchive(ss); // Create an input archive
		iarchive(s_out); // Read the data from the archive
	}
	EXPECT_EQ(s_in, s_out);
}

TEST(DataSerializingTest, Int) {
	stringstream ss;
	int s_in, s_out;

	s_in = 86765;
	{
		cereal::BinaryOutputArchive oarchive(ss);
		oarchive(s_in);
	}
	{
		cereal::BinaryInputArchive iarchive(ss); // Create an input archive
		iarchive(s_out); // Read the data from the archive
	}
	EXPECT_EQ(s_in, s_out);

}

TEST(DataSerializingTest, Class) {
	stringstream ss;
	Data s_in, s_out;

	s_in = Data();
	{
		cereal::BinaryOutputArchive oarchive(ss);
		oarchive(s_in);
	}
	{
		cereal::BinaryInputArchive iarchive(ss); // Create an input archive
		iarchive(s_out); // Read the data from the archive
	}
	EXPECT_EQ(s_in, s_out);

	s_in.increment();
	{
		cereal::BinaryOutputArchive oarchive(ss);
		oarchive(s_in);
	}
	{
		cereal::BinaryInputArchive iarchive(ss); // Create an input archive
		iarchive(s_out); // Read the data from the archive
	}
	EXPECT_EQ(s_in, s_out);

	{
		cereal::BinaryOutputArchive oarchive(ss);
		oarchive(s_in);
	}
	s_in.increment();
	{
		cereal::BinaryInputArchive iarchive(ss); // Create an input archive
		iarchive(s_out); // Read the data from the archive
	}
	EXPECT_NE(s_in, s_out);
}

TEST(DataSerializingTest, SharedPointer) {
	stringstream ss;

	{
		std::shared_ptr<int> s_in, s_out;
		s_in = std::make_shared<int>(9878765);
		{
			cereal::BinaryOutputArchive oarchive(ss);
			oarchive(s_in);
		}
		{
			cereal::BinaryInputArchive iarchive(ss); // Create an input archive
			iarchive(s_out); // Read the data from the archive
		}
		EXPECT_EQ(*s_in, *s_out);
	}

	{
		auto s_in = std::make_shared<Data>();
		std::shared_ptr<Data> s_out;
		{
			cereal::BinaryOutputArchive oarchive(ss);
			oarchive(s_in);
		}
		{
			cereal::BinaryInputArchive iarchive(ss); // Create an input archive
			iarchive(s_out); // Read the data from the archive
		}
		EXPECT_EQ(*s_in, *s_out);

	}

}

TEST(DataSerializingTest, ClassWithSharedPtr) {
	stringstream ss;

	{
		Data d = Data();
		Serializable s_in, s_out;
		d.increment();
		s_in = Serializable(54, "Test", d);
		{
			cereal::BinaryOutputArchive oarchive(ss);
			oarchive(s_in);
		}
		{
			cereal::BinaryInputArchive iarchive(ss); // Create an input archive
			iarchive(s_out); // Read the data from the archive
		}
		EXPECT_EQ(s_in, s_out);

	}

	{
		cereal::BinaryOutputArchive oarchive(ss);
		cereal::BinaryInputArchive iarchive(ss); // Create an input archive
		Data d = Data();
		Serializable s_in, s_out, s_in2, s_out2;
		d.increment();
		s_in = Serializable(54, "Test", d);
		{
			cereal::BinaryOutputArchive oarchive(ss);
			oarchive(s_in);
		}
		d.increment();
		s_in2 = Serializable(542, "Test2", d);
		{
			cereal::BinaryOutputArchive oarchive(ss);
			oarchive(s_in2);
		}
		{
			cereal::BinaryInputArchive iarchive(ss); // Create an input archive
			iarchive(s_out2); // Read the data from the archive
			iarchive(s_out); // Read the data from the archive
		}
		EXPECT_EQ(s_in2, s_out);
		EXPECT_EQ(s_in, s_out2);
	}
}

TEST(DataSerializingTest, ToFile) {
	stringstream ss;
	int s_in, s_out;

	s_in = 86765;
	{
		std::ofstream ofs;
		ofs.open("test.txt", std::ofstream::out | std::ofstream::binary);
		cereal::BinaryOutputArchive oarchive(ofs);
		oarchive(s_in);
		ofs.close();
	}
	{
		std::ifstream ifs;

		ifs.open("test.txt", std::ifstream::in | std::ifstream::binary);

		cereal::BinaryInputArchive iarchive(ifs); // Create an input archive
		iarchive(s_out); // Read the data from the archive
		ifs.close();
	}
	EXPECT_EQ(s_in, s_out);

}

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
