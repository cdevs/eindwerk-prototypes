/*
 * Serializable.h
 *
 *  Created on: Feb 12, 2015
 *      Author: uauser
 */

#ifndef SERIALIZABLE_H_
#define SERIALIZABLE_H_

#include <cereal/types/polymorphic.hpp>
#include <cereal/archives/binary.hpp>
#include "cereal/types/map.hpp"
#include "cereal/types/vector.hpp"
#include "cereal/types/memory.hpp"
#include "cereal/types/string.hpp"
#include "cereal/types/base_class.hpp"
#include <string>
#include <memory>

class Data {
public:
	Data() :
			_count(0) {
	}

	Data(int c):_count(c){};
	virtual ~Data() {
	}
	template<class Archive>
	void serialize(Archive & archive) {
		archive(_count); // serialize things by passing them to the archive
	}
	bool operator==(const Data& other) const {
		if (_count != other._count)
			return false;
		return true;
	}
	bool operator!=(const Data& other) const {
		return !(*this == other);
	}
	void increment() {
		++_count;
	}
private:
	friend class cereal::access;
	int _count;
};

class Serializable {
public:
	Serializable() :
			_id(0), _name("Default") {
	}
	Serializable(int id, std::string name, Data data) :
			_id(id), _name(name) {
		_data = std::make_shared<Data>(data);
	}
	virtual ~Serializable();
	template<class Archive>
	void serialize(Archive & archive) {
		archive(_id,
				_name,
				_data //TODO: Does not work as shared pointer
				); // serialize things by passing them to the archive
	}
	bool operator==(const Serializable& other) const {
		if (_id != other._id)
			return false;
		if (_name != other._name)
			return false;
		if (*_data != *other._data)
			return false;
		return true;
	}
	bool operator!=(const Serializable& other) const {
			return !(*this == other);
		}
private:
	friend class cereal::access;
	int _id;
	std::string _name;
	std::shared_ptr<Data> _data;
};

#endif /* SERIALIZABLE_H_ */
