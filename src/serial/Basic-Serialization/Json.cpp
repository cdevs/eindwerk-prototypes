//============================================================================
// Name        : Serializing.cpp
// Author      : 
// Version     :
// Copyright   : 
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <fstream>
#include "cereal/cereal.hpp"
#include <cereal/archives/binary.hpp>
#include <cereal/archives/json.hpp>
#include <cereal/archives/xml.hpp>
#include "cereal/types/map.hpp"
#include "cereal/types/vector.hpp"
#include "cereal/types/memory.hpp"
#include "cereal/types/string.hpp"
#include "cereal/types/base_class.hpp"
#include <string>
#include <memory>

#include <sstream>
#include "Serializable.h"

#include "gtest/gtest.h"

using namespace std;

TEST(DataSerializingTest, String) {
	stringstream ss;
	std::string s_in, s_out;

	s_in = "Test";
	{
		cereal::JSONOutputArchive oarchive(ss);
		oarchive(s_in);
	}
	{
		cereal::JSONInputArchive iarchive(ss); // Create an input archive
		iarchive(s_out); // Read the data from the archive
	}
	EXPECT_EQ(s_in, s_out);

	//JSONInputArchive sets the EOF error bit of ss, this needs to be cleared to continue
	ss.clear();
	s_in = "";
	{
		cereal::JSONOutputArchive oarchive(ss);
		oarchive(s_in);
	}
	{
		cereal::JSONInputArchive iarchive(ss); // Create an input archive
		iarchive(s_out); // Read the data from the archive
	}
	EXPECT_EQ(s_in, s_out);
}

TEST(DataSerializingTest, Int) {
	stringstream ss;
	int s_in, s_out;

	s_in = 86765;
	{
		cereal::JSONOutputArchive oarchive(ss);
		oarchive(s_in);
	}
	{
		cereal::JSONInputArchive iarchive(ss); // Create an input archive
		iarchive(s_out); // Read the data from the archive
	}
	EXPECT_EQ(s_in, s_out);

}

TEST(DataSerializingTest, Class) {
	stringstream ss;
	Data s_in, s_out;

	s_in = Data();
	{
		cereal::JSONOutputArchive oarchive(ss);
		oarchive(s_in);
	}
	{
		cereal::JSONInputArchive iarchive(ss); // Create an input archive
		iarchive(s_out); // Read the data from the archive
	}
	EXPECT_EQ(s_in, s_out);
	ss.clear();
	s_in.increment();
	{
		cereal::JSONOutputArchive oarchive(ss);
		oarchive(s_in);
	}
	{
		cereal::JSONInputArchive iarchive(ss); // Create an input archive
		iarchive(s_out); // Read the data from the archive
	}
	EXPECT_EQ(s_in, s_out);

	ss.clear();
	{
		cereal::JSONOutputArchive oarchive(ss);
		oarchive(s_in);
	}
	s_in.increment();
	{
		cereal::JSONInputArchive iarchive(ss); // Create an input archive
		iarchive(s_out); // Read the data from the archive
	}
	EXPECT_NE(s_in, s_out);
}

TEST(DataSerializingTest, SharedPointer) {
	stringstream ss;

	{
		std::shared_ptr<int> s_in, s_out;
		s_in = std::make_shared<int>(9878765);
		{
			cereal::JSONOutputArchive oarchive(ss);
			oarchive(s_in);
		}
		{
			cereal::JSONInputArchive iarchive(ss); // Create an input archive
			iarchive(s_out); // Read the data from the archive
		}
		EXPECT_EQ(*s_in, *s_out);
	}

	ss.clear();
	{
		auto s_in = std::make_shared<Data>();
		std::shared_ptr<Data> s_out;
		{
			cereal::JSONOutputArchive oarchive(ss);
			oarchive(s_in);
		}
		{
			cereal::JSONInputArchive iarchive(ss); // Create an input archive
			iarchive(s_out); // Read the data from the archive
		}
		EXPECT_EQ(*s_in, *s_out);

	}

}

TEST(DataSerializingTest, ClassWithSharedPtr) {
	stringstream ss;

	{
		Data d = Data();
		Serializable s_in, s_out;
		d.increment();
		s_in = Serializable(54, "Test", d);
		{
			cereal::JSONOutputArchive oarchive(ss);
			oarchive(s_in);
		}
		{
			cereal::JSONInputArchive iarchive(ss); // Create an input archive
			iarchive(s_out); // Read the data from the archive
		}
		EXPECT_EQ(s_in, s_out);

	}

	ss.clear();
	{
		Data d = Data();
		Serializable s_in, s_out, s_in2, s_out2;
		d.increment();
		s_in = Serializable(54, "Test", d);
		{
			cereal::JSONOutputArchive oarchive(ss);
			oarchive(s_in);
			//}
			d.increment();
			s_in2 = Serializable(542, "Test2", d);
			/*		{
			 cereal::JSONOutputArchive oarchive(ss);
			 */oarchive(s_in2);
		}
		{
			cereal::JSONInputArchive iarchive(ss); // Create an input archive
			iarchive(s_out2); // Read the data from the archive
			iarchive(s_out); // Read the data from the archive
		}
		EXPECT_EQ(s_in2, s_out);
		EXPECT_EQ(s_in, s_out2);
	}
}

TEST(DataSerializingTest, ToFile) {
	Data d = Data();
	Serializable s_in, s_out;
	d.increment();
	s_in = Serializable(54, "Test", d);
	//JSONOutputArchive writes to stream at destruction
	std::ofstream ofs("test.json");
	{
		cereal::JSONOutputArchive oarchive(ofs);
		oarchive(s_in);
	}
	ofs.close();

	{
		std::ifstream ifs;
		ifs.open("test.json");
		cereal::JSONInputArchive iarchive(ifs); // Create an input archive
		iarchive(s_out); // Read the data from the archive
		ifs.close();
	}
	EXPECT_EQ(s_in, s_out);

}

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
