//============================================================================
// Name        : Serializing.cpp
// Author      : 
// Version     :
// Copyright   : 
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <fstream>
#include "cereal/cereal.hpp"
#include <cereal/archives/binary.hpp>
#include <cereal/archives/xml.hpp>
#include "cereal/types/map.hpp"
#include "cereal/types/vector.hpp"
#include "cereal/types/memory.hpp"
#include "cereal/types/string.hpp"
#include "cereal/types/base_class.hpp"
#include <string>
#include <memory>

#include <sstream>
#include "Serializable.h"

#include "gtest/gtest.h"

using namespace std;

TEST(DataSerializingTest, String) {
	stringstream ss;
	std::string s_in, s_out;

	s_in = "Test";
	{
		cereal::XMLOutputArchive oarchive(ss);
		oarchive(s_in);
	}
	{
		cereal::XMLInputArchive iarchive(ss); // Create an input archive
		iarchive(s_out); // Read the data from the archive
	}
	EXPECT_EQ(s_in, s_out);

	s_in = "";
	{
		cereal::XMLOutputArchive oarchive(ss);
		oarchive(s_in);
	}
	{
		cereal::XMLInputArchive iarchive(ss); // Create an input archive
		iarchive(s_out); // Read the data from the archive
	}
	EXPECT_EQ(s_in, s_out);
}

TEST(DataSerializingTest, Int) {
	stringstream ss;
	int s_in, s_out;

	s_in = 86765;
	{
		cereal::XMLOutputArchive oarchive(ss);
		oarchive(s_in);
	}
	{
		cereal::XMLInputArchive iarchive(ss); // Create an input archive
		iarchive(s_out); // Read the data from the archive
	}
	EXPECT_EQ(s_in, s_out);

}

TEST(DataSerializingTest, Class) {
	stringstream ss;
	Data s_in, s_out;

	s_in = Data();
	{
		cereal::XMLOutputArchive oarchive(ss);
		oarchive(s_in);
	}
	{
		cereal::XMLInputArchive iarchive(ss); // Create an input archive
		iarchive(s_out); // Read the data from the archive
	}
	EXPECT_EQ(s_in, s_out);

	s_in.increment();
	{
		cereal::XMLOutputArchive oarchive(ss);
		oarchive(s_in);
	}
	{
		cereal::XMLInputArchive iarchive(ss); // Create an input archive
		iarchive(s_out); // Read the data from the archive
	}
	EXPECT_EQ(s_in, s_out);

	{
		cereal::XMLOutputArchive oarchive(ss);
		oarchive(s_in);
	}
	s_in.increment();
	{
		cereal::XMLInputArchive iarchive(ss); // Create an input archive
		iarchive(s_out); // Read the data from the archive
	}
	EXPECT_NE(s_in, s_out);
}

TEST(DataSerializingTest, SharedPointer) {
	stringstream ss;

	{
		std::shared_ptr<int> s_in, s_out;
		s_in = std::make_shared<int>(9878765);
		{
			cereal::XMLOutputArchive oarchive(ss);
			oarchive(s_in);
		}
		{
			cereal::XMLInputArchive iarchive(ss); // Create an input archive
			iarchive(s_out); // Read the data from the archive
		}
		EXPECT_EQ(*s_in, *s_out);
	}

	{
		auto s_in = std::make_shared<Data>();
		std::shared_ptr<Data> s_out;
		{
			cereal::XMLOutputArchive oarchive(ss);
			oarchive(s_in);
		}
		{
			cereal::XMLInputArchive iarchive(ss); // Create an input archive
			iarchive(s_out); // Read the data from the archive
		}
		EXPECT_EQ(*s_in, *s_out);

	}

}

TEST(DataSerializingTest, ClassWithSharedPtr) {
	stringstream ss;

	{
		Data d = Data();
		Serializable s_in, s_out;
		d.increment();
		s_in = Serializable(54, "Test", d);
		{
			cereal::XMLOutputArchive oarchive(ss);
			oarchive(s_in);
		}
		{
			cereal::XMLInputArchive iarchive(ss); // Create an input archive
			iarchive(s_out); // Read the data from the archive
		}
		EXPECT_EQ(s_in, s_out);

	}

	{
		Data d = Data();
		Serializable s_in, s_out, s_in2, s_out2;
		d.increment();
		s_in = Serializable(54, "Test", d);
		{
			cereal::XMLOutputArchive oarchive(ss);
			oarchive(s_in);
			//}
			d.increment();
			s_in2 = Serializable(542, "Test2", d);
			/*		{
			 cereal::XMLOutputArchive oarchive(ss);
			 */oarchive(s_in2);
		}
		{
			cereal::XMLInputArchive iarchive(ss); // Create an input archive
			iarchive(s_out2); // Read the data from the archive
			iarchive(s_out); // Read the data from the archive
		}
		EXPECT_EQ(s_in2, s_out);
		EXPECT_EQ(s_in, s_out2);
	}
}

TEST(DataSerializingTest, ToFile) {
	Data d = Data();
	Serializable s_in, s_out;
	d.increment();
	s_in = Serializable(54, "Test", d);
	//XMLOutputArchive writes to stream at destruction
	std::ofstream ofs("test.xml");
	{
		cereal::XMLOutputArchive oarchive(ofs);
		oarchive(s_in);
	}
	ofs.close();

	{
		std::ifstream ifs;
		ifs.open("test.xml");
		cereal::XMLInputArchive iarchive(ifs); // Create an input archive
		iarchive(s_out); // Read the data from the archive
		ifs.close();
	}
	EXPECT_EQ(s_in, s_out);

}

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
