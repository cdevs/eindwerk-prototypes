/*
 * main.cpp
 *
 *  Created on: Feb 12, 2015
 *      Author: paul
 */

#include "Board.h"
#include <cereal/archives/binary.hpp>
#include <cereal/archives/xml.hpp>
#include <cereal/archives/json.hpp>
#include <cereal/archives/portable_binary.hpp>
#include <fstream>
#include <iostream>
#include <string>
#include "gtest/gtest.h"

template<typename Archive>
void save(Gameoflife::Board& b) {
	std::ofstream ofs;
	std::string name = "it_" + std::to_string(b.getIterations());	
	ofs.open(name);
	{
		Archive oar(ofs);
		oar(b);
	}
	ofs.close();
}

 TEST(GameOfLife, StateRestoring) {
	Gameoflife::Board b;
	save<cereal::PortableBinaryOutputArchive>(b);
	while (!b.isEndgame()) {
		b.iterate();
		b.update();
		if (b.getIterations() % 5 == 0) {
			save<cereal::PortableBinaryOutputArchive>(b);
		}
	}
	std::stringstream normal_end;
	b.print(normal_end);
	save<cereal::PortableBinaryOutputArchive>(b);

	std::ifstream ifs("it_50");
	cereal::PortableBinaryInputArchive iar(ifs);
	iar(b);
	while (!b.isEndgame()) {
		b.iterate();
		b.update();
	}
	std::stringstream restored_end;
	b.print(restored_end);
	EXPECT_EQ(restored_end.str(),normal_end.str());
}


 int main(int argc, char **argv) {
 ::testing::InitGoogleTest(&argc, argv);
 return RUN_ALL_TESTS();
 }

