/*
 * Board.h
 *
 *  Created on: Feb 12, 2015
 *      Author: paul
 */

#ifndef BOARD_H_
#define BOARD_H_

#include <fstream>
#include <cereal/cereal.hpp>
#include <vector>
#include <iostream>
#include <cereal/archives/binary.hpp>
#include <cereal/archives/xml.hpp>
#include <cereal/archives/json.hpp>
#include <cereal/archives/portable_binary.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/base_class.hpp>


namespace Gameoflife {

class Board {
public:
	Board();
	virtual ~Board();
	void print(std::ostream& out);
	void iterate();
	void update();

	int numberOfNeighbors(int i, int j);
	bool alive(int i, int j);
	bool isEndgame() const {
		return endgame;
	}

	void setEndgame(bool endgame) {
		this->endgame = endgame;
	}

	int getIterations() const;

	template<class Archive>
	void serialize(Archive& archive){
		archive(grid, dead, life, _iterations, endgame);
	}

private:
	friend class cereal::access;
	//bool grid[20][20];
	std::vector<std::vector<bool> > grid;
	//bool dead[20][20];
	std::vector<std::vector<bool> > dead;
	//bool life[20][20];
	std::vector<std::vector<bool> > life;
	int _iterations;
	bool endgame;


};

} /* namespace Gameoflife */

#endif /* BOARD_H_ */
