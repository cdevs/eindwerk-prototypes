/*
 * Board.cpp
 *
 *  Created on: Feb 12, 2015
 *      Author: paul
 */

#include "Board.h"

namespace Gameoflife {

Board::Board() {
	// Setup the board with Default seed:
	_iterations = 0;
	grid = std::vector<std::vector<bool> >(20,std::vector<bool>(20, false));
	dead = std::vector<std::vector<bool> >(20,std::vector<bool>(20, false));
	life = std::vector<std::vector<bool> >(20,std::vector<bool>(20, false));
	for(int i = 0; i < 20;i++){
		for(int j = 0; j < 20; j++){
			this->grid[i][j] = false;
			this->dead[i][j] = false;
			this->life[i][j] = false;
		}
	}

	grid[2][2] = true;
	grid[3][3] = true;
	grid[4][1] = true;
	grid[4][2] = true;
	grid[4][3] = true;
    endgame = false;
}

void Board::print(std::ostream& out){
	out << "Printing Iteration: " << _iterations << std::endl;
	for(int i = 0; i < 20;i++){
		for(int j = 0; j < 20; j++){
			if(grid[i][j] == false){
				out << "0";
			}
			else
			{
				out << "X";
			}
		}
		out << std::endl;
	}
	out << std::endl;

}

int Board::numberOfNeighbors(int i, int j){
	int k = 0;
	if (i == 0 || i == 19 || j == 0 || j == 19){
		return 0;
	}
	if(grid[i+1][j] == true) k++;
	if(grid[i+1][j-1] == true) k++;
	if(grid[i+1][j+1] == true) k++;
	if(grid[i-1][j] == true) k++;
	if(grid[i-1][j-1] == true) k++;
	if(grid[i-1][j+1] == true) k++;
	if(grid[i][j+1] == true) k++;
	if(grid[i][j-1] == true) k++;
	return k;
}

bool Board::alive(int i, int j){
	return grid[i][j];
}

void Board::iterate()
{
	_iterations++;
	for(int i = 0; i < 20;i++){
		for(int j = 0; j < 20; j++){
			int k = this->numberOfNeighbors(i,j);
			if(this->alive(i,j)){
				if(k < 2) dead[i][j] = true;
				if(k > 3) dead[i][j] = true;
				if(k == 2 || k == 3) dead[i][j] = false;
//				std::cout << "Altering Board!" << std::endl;
			}
			else{
//				std::cout << "Reviving a dead cell" << std::endl;
//				std::cout << "Dead cells" << std::endl;
				if(k==3) life[i][j] = true;
			}

		}
	}
}

void Board::update(){
//	std::cout << "Entering UPDATE" << std::endl;
	endgame = true;

	for(int i = 0; i < 20;i++){
			for(int j = 0; j < 20; j++){
//				std::cout << "Updating Board" << std::endl;
				if(dead[i][j]) {
					grid[i][j] = false;
					endgame = false;
				}
				if(life[i][j]){
					grid[i][j] = true;
					endgame = false;
				}
			}
		}
	for(int i = 0; i < 20;i++){
			for(int j = 0; j < 20; j++){
//				std::cout << "Updating Board00" << std::endl;
				this->dead[i][j] = false;
				this->life[i][j] = false;
			}
		}

}

Board::~Board() {
	// TODO Auto-generated destructor stub
}

int Board::getIterations() const {
	return _iterations;
}

} /* namespace Gameoflife */
