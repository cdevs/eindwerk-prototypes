#ifndef PRIORITY_QUEUE_H_
#define PRIORITY_QUEUE_H_

#include <queue>
#include <mutex>
#include <condition_variable>

template <class T, class Container = std::vector<T>, class Compare = std::less<typename Container::value_type> >
class PriorityQueue {
public:
	PriorityQueue(Compare);
	virtual ~PriorityQueue();

	T pop();
	void push(const T&);
private:
	std::priority_queue<T, Container, Compare> _stdPriorityQueue;
	std::mutex _mutex;
	std::condition_variable _conditionVariable;
};

template <class T, class Container, class Compare>
PriorityQueue<T, Container, Compare>::PriorityQueue(Compare compareFunction) {
	_stdPriorityQueue = std::priority_queue<T, Container, Compare>(compareFunction);
}

template <class T, class Container, class Compare>
PriorityQueue<T, Container, Compare>::~PriorityQueue() {
}

template <class T, class Container, class Compare>
T PriorityQueue<T, Container, Compare>::pop() {
	// acquire lock
	std::unique_lock< std::mutex > lock(_mutex);

	// pause thread until priority queue is not empty
	while (_stdPriorityQueue.empty())
		_conditionVariable.wait(lock);

	// get top element
	T popElement = _stdPriorityQueue.top();

	// pop off element
	_stdPriorityQueue.pop();

	return popElement;
}

template <class T, class Container, class Compare >
void PriorityQueue<T, Container, Compare>::push(const T& item) {
	// acquire lock
	std::unique_lock< std::mutex > lock(_mutex);

	// push item
	_stdPriorityQueue.push(item);

	// release lock
	lock.unlock();

	// resume a thread
	_conditionVariable.notify_one();
}
#endif