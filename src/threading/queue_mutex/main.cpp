#include <iostream>
#include <vector>
#include <thread>

#include "PriorityQueue.h"
#include "Message.h"
#include "ThreadPool.h"

int main() {
	// Compare priority message function
	std::function<bool(Message, Message)> comp = [](Message a, Message b){ return a.getPriority() > b.getPriority();  };


	//////////////////////////////////////////////
	// Execute main thread test
	//////////////////////////////////////////////

	// Create message priority queue
	PriorityQueue<Message, std::vector<Message>, decltype(comp)> pretestQueue(comp);

	// Add low priority message
	Message changeWeatherMessage(5, "[LOW] Change weather");
	pretestQueue.push(changeWeatherMessage);

	// Add high priority message
	Message switchLightMessage(0, "[HIGH] Switch light");
	pretestQueue.push(switchLightMessage);

	// Output top message
	Message message = pretestQueue.pop();
	std::cout << message.getMessage().c_str() << std::endl;
	// EXPECTED OUTPUT = [HIGH] Switch light


	//////////////////////////////////////////////
	// Execute multi-thread test
	//////////////////////////////////////////////

	// Create message priority queue
	PriorityQueue<Message, std::vector<Message>, decltype(comp)> messageQueue(comp);
	
	// Create thread pool
	ThreadPool* threadPool = new ThreadPool(4);
	
	// Add task to enqueue high priority message, delayed by CPU intense for loop
	threadPool->enqueue([&messageQueue]{
		// CPU intense for loop
		volatile unsigned long long i;
		for (i = 0; i < 1000000000ULL; ++i);

		// Enqueue high priority message
		Message switchLightMessage(0, "[HIGH] Switch light");
		messageQueue.push(switchLightMessage);
	});

	// Add task to enqueue low priority message (should be enqueued faster than the high priority message, since it is not delayed)
	threadPool->enqueue([&messageQueue]{
		Message changeWeatherMessage(5, "[LOW] Change weather");
		messageQueue.push(changeWeatherMessage);
	});

	// Add task to print the pop of the message queue
	threadPool->enqueue([&messageQueue]{
		Message message = messageQueue.pop();
		std::cout << message.getMessage().c_str() << std::endl;
	});
	// EXPECTED OUTPUT = [LOW] Change weather

	// Add task to enqueue medium priority message
	threadPool->enqueue([&messageQueue]{
		Message fireMessage(10, "[MEDIUM] Fire");
		messageQueue.push(fireMessage);
	});

	std::chrono::milliseconds duration(2000);
	std::this_thread::sleep_for(duration);

	delete threadPool;

	return 0;
}