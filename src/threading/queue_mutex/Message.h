#ifndef MESSAGE_H_
#define MESSAGE_H_

#include <string>

class Message {
public:
	Message(int priority, std::string message) {
		_priority = priority;
		_message = message;
	}
	virtual ~Message() {

	}

	int getPriority() { return _priority; }
	std::string getMessage() { return _message; }
private:
	int _priority;
	std::string _message;
};

#endif