#include <iostream>
#include <thread>

int main() {
	auto start = std::chrono::steady_clock::now();

	// queue a bunch
	for (int i = 0; i < 10; ++i) {
		std::thread t([] {
			volatile unsigned long long i;
			for (i = 0; i < 1000000000ULL; ++i);
		});
		t.join();
	}

	auto end = std::chrono::steady_clock::now();

	// Store the time difference between start and end
	auto diff = end - start;

	std::cout << std::chrono::duration< double, std::milli >(diff).count() << " ms" << std::endl;

	return 0;
}
