#include <queue>
#include <vector>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <chrono>

class ThreadPool;

class Worker {
public:
	Worker(ThreadPool*);
	~Worker();

	void operator()();

private:
	ThreadPool* _threadPool;
};


class ThreadPool {
public:
	ThreadPool(size_t);
	virtual ~ThreadPool();

	template<class F>
	void enqueue(F f);
	std::function< void() > popTask();

	void wait(std::unique_lock< std::mutex >&);

	std::mutex& getMutexReference();

	bool isStopping();
	bool hasTasks();

	void setStartTime(std::chrono::steady_clock::time_point);
private:
	std::vector< std::thread > _workers;
	std::queue< std::function< void() > > _tasks;

	std::mutex _queue_mutex;
	std::condition_variable _condition;

	std::chrono::steady_clock::time_point _startTime;

	bool _stop;
};

Worker::Worker(ThreadPool* threadPool) {
	this->_threadPool = threadPool;

}

void Worker::operator()() {
	std::function< void() > task;

	while (true) {
			{
				//enter critical (lock)
				std::unique_lock< std::mutex > lock(_threadPool->getMutexReference());

				while (!_threadPool->isStopping() && !_threadPool->hasTasks()) {
					_threadPool->wait(lock);
				}

				if (_threadPool->isStopping())
					return;

				task = _threadPool->popTask();
				//leave critical
			}
		task();
	}
}


Worker::~Worker()
{
}

/**
* Constructor
*
* @param nWorkers	Amount of workers in the threadpool
*
*/
ThreadPool::ThreadPool(size_t nWorkers) {
	// create workers
	for (size_t i = 0; i < nWorkers; i++) {
		// create the worker thread

		// add the worker to out workers
		_workers.push_back(std::thread(Worker(this)));
	}
}

/**
* Destructor
*
*/
ThreadPool::~ThreadPool() {
	// stop all threads
	_stop = true;
	_condition.notify_all();

	for (size_t i = 0; i < _workers.size(); i++) {
		_workers.at(i).join();
	}
	auto end = std::chrono::steady_clock::now();

	// Store the time difference between start and end
	auto diff = end - _startTime;

	std::cout << std::chrono::duration< double, std::milli >(diff).count() << " ms" << std::endl;
}

/**
* Enqueues a task to the threadpool
*
* All enqueued tasks will be handled by a series of workers (threads)
*
* @param task	The task to enqueue
*
*/
template<class F>
void ThreadPool::enqueue(F f) {
		{
			// acquire lock
			std::unique_lock< std::mutex > lock(_queue_mutex);

			// add the task
			_tasks.push(std::function< void() >(f));

			//release lock
		}
	// call a thread
	_condition.notify_one();
}

/**
* Makes the thread pool wait (when there are no tasks)
*
* @param lock	The lock to use to block threads until condition variable is notified.
*/

void ThreadPool::wait(std::unique_lock< std::mutex >& lock) {
	this->_condition.wait(lock);
}

/**
* Checks if ThreadPool is stopping
*
* @returns	True if the ThreadPool is stopping, false if it isn't
*/
bool ThreadPool::isStopping() {
	return _stop;
}

/**
* Checks if ThreadPool has tasks
*
* @returns	True if the ThreadPool has tasks, false if it doesn't
*/
bool ThreadPool::hasTasks() {
	return !(_tasks.empty());
}

/**
* Pops a task from the queue
*
* @returns	The task in front of the queue
*/
std::function< void() > ThreadPool::popTask() {
	// get the front task
	std::function< void() > task = this->_tasks.front();

	// pop it off
	this->_tasks.pop();

	// return it
	return task;
}

/**
* Get mutex reference
*
* @returns	A reference to the mutex object of the queue
*/
std::mutex& ThreadPool::getMutexReference() {
	return this->_queue_mutex;
}

void ThreadPool::setStartTime(std::chrono::steady_clock::time_point startTime) {
	this->_startTime = startTime;
}