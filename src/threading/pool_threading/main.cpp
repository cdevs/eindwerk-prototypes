#include <iostream>
#include <vector>
#include <chrono>

#include "ThreadPool.h"

int main() {
	// create a thread pool of 4 worker threads
	ThreadPool pool(4);

	auto start = std::chrono::steady_clock::now();

	pool.setStartTime(start);

	// queue a bunch
	for (int i = 0; i < 10; ++i) {
		pool.enqueue([] {
			volatile unsigned long long i;
			for (i = 0; i < 1000000000ULL; ++i);
		});
	}
	auto end = std::chrono::steady_clock::now();

	// Store the time difference between start and end
	auto diff = end - start;

	std::cout << std::chrono::duration< double, std::milli >(diff).count() << " ms" << std::endl;
	

	return 0;
}
