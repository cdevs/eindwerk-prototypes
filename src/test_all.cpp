/*
 * test_all.cpp
 *
 *  Created on: 26 Feb 2015
 *      Author: jweiren
 */

#include "gtest/gtest.h"

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}


