/*
 * testmodel2.h
 *
 * This is the second test model.
 *
 *  Created on: 15-feb.-2015
 *      Author: david
 */
#include "model.h"

namespace cdevs
{
  class TestModel2 : public Model
  {  
  public:
    // A function to do something, so we can demonstrate the plugin
    std::string toString() const
    {
      return std::string("This is model two.");
    }
  };
}

extern "C"
{
	// Function to return an instance of a new TestModel1 object
#if defined(_MSC_VER) // Microsoft compiler
	_declspec(dllexport)
#endif
	cdevs::Model* construct()
	{
		return new cdevs::TestModel2();
	}
}
