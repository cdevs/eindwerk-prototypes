/*
 * tests.cpp
 *
 * This source file contains all unit tests to confirm
 * that the models are loaded and handled as specified.
 *
 *  Created on: 15-feb.-2015
 *      Author: david
 */

#include "modelloader.h"
#include <iostream>
#include <string>
#include <vector>

#include "gtest/gtest.h"

using namespace cdevs;
using namespace std;

class ModelTest : public ::testing::Test {
};

// Tests the call to 'toString()' on the first testmodel
TEST_F(ModelTest, model1) {
  char* filepath = "../build/src/modelloader/libtestmodel1.so";
  // Alert that we are attempting to load a model
  //std::cout << "Loading model \"" << filepath << "\"" << std::endl;
  Model* model = loadModel(filepath);
  ASSERT_FALSE(model==NULL);
  //std::cout << "Calling 'toString()' on model 1." << std::endl;
  //std::cout << "[Model " << filepath << "] " << model->toString() << std::endl;
  EXPECT_EQ(model->toString(), "This is model one.");
}

// Tests the call to 'toString()' on the second testmodel
TEST_F(ModelTest, model2) {
  char* filepath = "../build/src/modelloader/libtestmodel2.so";
  // Alert that we are attempting to load a model
  //std::cout << "Loading model \"" << filepath << "\"" << std::endl;
  Model* model = loadModel(filepath);
  ASSERT_FALSE(model==NULL);
  //std::cout << "Calling 'toString()' on model 2." << std::endl;
  //std::cout << "[Model " << filepath << "] " << model->toString() << std::endl;
  EXPECT_EQ(model->toString(), "This is model two.");
}

// Tests the call to 'toString()' on a non-existing model file
TEST_F(ModelTest, model3) {
  char* filepath = "../build/src/modelloader/libtestmodel3.so";
  // Alert that we are attempting to load a model
  //std::cout << "Loading model \"" << filepath << "\"" << std::endl;
  Model* model = loadModel(filepath);
  ASSERT_TRUE(model==NULL);
}

