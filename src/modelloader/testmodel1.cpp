/*
 * testmodel1.h
 *
 * This is the first test model.
 *
 *  Created on: 15-feb.-2015
 *      Author: david
 */
#include "model.h"

namespace cdevs
{
  class TestModel1 : public Model
  {  
  public:
    // A function to do something, so we can demonstrate the plugin
    std::string toString() const
    {
      return std::string("This is model one.");
    }
  };
}

extern "C" 
{
	// Function to return an instance of a new TestModel1 object
	#if defined(_MSC_VER) // Microsoft compiler
		_declspec(dllexport)
	#endif
	cdevs::Model* construct()
	{
	return new cdevs::TestModel1();
	}
}
