/*
 * model.h
 *
 * This is the interface for the models to be loaded by the system.
 *
 *  Created on: 15-feb.-2015
 *      Author: david
 */
#ifndef _MODEL_H_
#define _MODEL_H_

#include <string>

#if defined(_MSC_VER) // Microsoft compiler
    #include <windows.h>
#elif defined(__GNUC__) // GNU compiler
    #include <dlfcn.h>
#else
	#error define your compiler
#endif

namespace cdevs
{
  class Model
  {
  private:
	  void* handle;
  public:
	void setHandle(void* h) { handle = h; };
	void* getHandle() const { return handle; };
    virtual std::string toString() const = 0;
    virtual ~Model() {
    	// handle its own unloading (this is the library itself).
    	if (handle != NULL)
    	{
			#if defined(_MSC_VER) // Microsoft compiler
    			FreeLibrary((HINSTANCE)handle);
			#elif defined(__GNUC__) // GNU compiler
				dlclose(handle);
			#endif
    	}
    };
  };
}

#endif // _MODEL_H_
