#include <iostream>
#include <vector>

#if defined(_MSC_VER) // Microsoft compiler
    #include <windows.h>
#elif defined(__GNUC__) // GNU compiler
    #include <dlfcn.h>
#else
	#error define your compiler
#endif

#include "modelloader.h"

cdevs::Model* loadModel(char* path)
{
	// Load the plugin's .so file
	void *handle = NULL;

	#if defined(_MSC_VER) // Microsoft compiler
        if(!(handle = (void*)LoadLibrary(path)))
        {
        	std::cerr << "Model: loading failed. (error code " << GetLastError() << ")" << std::endl;
        	return NULL;
        }
    #elif defined(__GNUC__) // GNU compiler
        if(!(handle = dlopen(path, RTLD_LAZY)))
		{
		  std::cerr << "Model: " << dlerror() << std::endl;
		  return NULL;
		}
		dlerror();
    #endif

	// Get the modelConstructor function
	cdevs::Model* (*construct)(void);
	#if defined(_MSC_VER) // Microsoft compiler
		construct = (cdevs::Model* (*)()) GetProcAddress((HMODULE)handle, "construct");
	#elif defined(__GNUC__) // GNU compiler
		construct = (cdevs::Model* (*)(void)) dlsym(handle, "construct");
	#endif
	char *error = NULL;

	#if defined(__GNUC__) // GNU compiler
		if((error = dlerror()))
		{
		  std::cerr << "Model: " << dlerror() << std::endl;
		  dlclose(handle);
		  return NULL;
		}
	#endif

	// Construct a model
	cdevs::Model * model = NULL;
	if (construct != NULL) {
		model = construct();
		model->setHandle(handle);
	}
	return model;
}
