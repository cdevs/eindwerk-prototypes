/*
 * modelloader.h
 *
 * This file contains the declaration for the model loader
 *
 *  Created on: 15-feb.-2015
 *      Author: david
 *
 */

#ifndef MODELLOADER_H_
#define MODELLOADER_H_

#include "model.h"

cdevs::Model* loadModel(char* path);

#endif /* MODELLOADER_H_ */
