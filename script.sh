#!bin/bash

echo "Building google test framework"

cd ../lib
cd gtest-1.7.0
rm -r CMakeCache.txt
cmake ./
make
cd ../../build

echo "done building google test framework"
